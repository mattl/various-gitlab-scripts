# "Getting to know you" -- a Ruby script to find team members to meet with
# Written in 2016 by Matt Lee <mattl@gitlab.com>
#
# To the extent possible under law, the author(s) have dedicated all
# copyright and related and neighboring rights to this software to the
# public domain worldwide. This software is distributed without any
# warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication
# along with this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

require 'yaml'
require 'open-uri'
require 'pp'

puts "Getting the current staff file from GitLab.com...\n\n"
current_staff = open('https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/team.yml'){|f|f.read}

puts "Finding you 10 random team members to meet with...\n\n"

begin
  teamdata = YAML.load(current_staff)
rescue
  puts "The YAML file in GitLab appears to have errors, please check and try again"
  puts "<https://gitlab.com/gitlab-com/www-gitlab-com/commits/master>"
end

# Find people who have their gitlab.com username in the file, make an
# array of those people and then take 10 random people as a sample...

started = teamdata.select { |e| e['gitlab'] != nil }.map { |f| f['gitlab'] }.sample(10)

started.each do |i|

  puts "You should meet with " + i
  
end

# TODO, it would be nice if we had access to their calendly accounts,
# perhaps we could add that to the YAML.
